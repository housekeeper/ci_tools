require 'ci_tools'

if defined?(Rails)
  require 'cucumber/rails'
  require 'email_spec/cucumber'
  require 'ci_tools/cucumber/email_steps'
else
  require 'cucumber'
end

require 'cucumber/rspec/doubles'
require 'cucumber/api_steps'
require 'webmock/cucumber'

require 'ci_tools/capybara'
require 'ci_tools/poltergeist'
require 'ci_tools/web_mock'

require 'ci_tools/cucumber/active_record'
require 'ci_tools/cucumber/javascript'
require 'ci_tools/cucumber/page_steps'
require 'ci_tools/cucumber/time'
require 'ci_tools/cucumber/time_steps'
require 'ci_tools/cucumber/type'
