require 'rubocop/rake_task'
require 'cucumber'
require 'cucumber/rake/task'
require 'rspec/core/rake_task'

load 'rails/tasks/annotations.rake' if defined? Rails

RuboCop::RakeTask.new
Cucumber::Rake::Task.new
RSpec::Core::RakeTask.new

namespace :ci_tools do
  dependencies = %i[rubocop]
  dependencies += %i[cucumber] if Dir.exist? 'features'
  dependencies += %i[spec] if Dir.exist? 'spec'
  dependencies = %i[environment] + dependencies + %i[notes] if defined? Rails
  desc 'Quick Check'
  task check: dependencies
  desc '.. DONE.'
end
