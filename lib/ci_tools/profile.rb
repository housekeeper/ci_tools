module CiTools
  class Profile
    class << self
      def capture(filename, &block)
        result = RubyProf.profile(&block)
        open(filename, 'w'.freeze) do |f|
          RubyProf::CallTreePrinter.new(result).print(f, min_percent: 1)
        end
      end
    end
  end
end
