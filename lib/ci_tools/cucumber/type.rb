module CiTools
  module Cucumber
    module Type
      def classify(type)
        type.split(' ').join('_').classify
      end
    end
  end
end

World(CiTools::Cucumber::Type)
