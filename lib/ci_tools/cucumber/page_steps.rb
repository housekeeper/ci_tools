Then(/^I should see "([^"]*)"$/) do |text|
  expect(page).to have_content(text)
end

Then(/^I should not see "(.*?)"$/) do |text|
  expect(page).not_to have_content(text)
end

Then(/^I see the page$/) do
  save_and_open_page
end

Then(/^I pause( to debug)?$/) do |debug|
  debug ? binding.pry : STDIN.getc
end
