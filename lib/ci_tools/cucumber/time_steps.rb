Given(/^today is "([^"]*)"$/) do |date|
  freeze_time date
end

Given(/^the time is "([^"]*)"$/) do |date|
  freeze_time date
end

When(/^I am in "([^"]+)" time zone$/) do |tz|
  Time.zone = tz
end

After do
  Timecop.return
end