After('@javascript') do
  if Capybara.current_driver == Capybara.javascript_driver && page.driver.respond_to?(:console_messages)
    error_messages = page.driver.console_messages
    raise "Javascript errors: #{error_messages}" if error_messages.positive?
  end
end
