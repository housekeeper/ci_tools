module CiTools
  module Cucumber
    module Time
      def freeze_time(date)
        time = if ::Time.zone
                 ::Time.zone.parse(date)
               else
                 ::Time.parse(date)
               end
        Timecop.freeze(time)
      end
    end
  end
end

World(CiTools::Cucumber::Time)
