
if defined?(Rails)
  require 'binding_of_caller'
  require 'better_errors'
  require 'email_spec'
  require 'rack-mini-profiler'
  require 'flamegraph'
  require 'stackprof'
end

require 'rubocop'
require 'brakeman'
require 'timecop'
require 'pry-byebug'
require 'awesome_print'
require 'mini_racer'
require 'selenium-webdriver'
require 'capybara'
require 'capybara/poltergeist'
require 'webmock/cucumber'
require 'database_cleaner'
require 'vcr'
require 'bullet'
require 'letter_opener'

require 'ci_tools/cucumber/active_record'
require 'ci_tools/cucumber/page_steps'
require 'ci_tools/cucumber/javascript'
require 'ci_tools/cucumber/time'
require 'ci_tools/cucumber/time_steps'
require 'ci_tools/cucumber/type'
require 'ci_tools/capybara'
require 'ci_tools/poltergeist'
require 'ci_tools/profile'
require 'ci_tools/web_mock'
require 'ci_tools/version'

module CiTools
end
