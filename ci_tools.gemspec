# coding: utf-8
lib = File.expand_path("../lib", __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require "ci_tools/version"

Gem::Specification.new do |spec|
  spec.name          = "ci_tools"
  spec.version       = CiTools::VERSION
  spec.authors       = ["housekeeper"]
  spec.email         = ["carl@house-keeping.com"]

  spec.summary       = %q{Tools for testing, standards and CI}
  spec.description   = %q{Tools for testing, standards and CI}
  spec.homepage      = ""
  spec.license       = "MIT"

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  if spec.respond_to?(:metadata)
    spec.metadata["allowed_push_host"] = "TODO: Set to 'http://mygemserver.com'"
  else
    raise "RubyGems 2.0 or newer is required to protect against " \
      "public gem pushes."
  end

  spec.files = Dir.glob('{app,lib,bin,config,spec}/**/*') + ['README.md']
  spec.executables = spec.files.grep(%r{^bin/}).map { |f| File.basename(f) }
  spec.test_files = spec.files.grep(%r{^(test|spec|features)/})
  
  spec.bindir        = "bin"
  spec.require_paths = ["lib"]

  spec.add_dependency 'rails', '~> 5'
  
  spec.add_dependency 'rubocop'
  spec.add_dependency 'brakeman'
  spec.add_dependency 'fasterer'
  spec.add_dependency 'cucumber-rails'
  spec.add_dependency 'cucumber-api-steps'
  spec.add_dependency 'selenium-webdriver'
  spec.add_dependency 'database_cleaner'
  spec.add_dependency 'capybara'
  spec.add_dependency 'poltergeist'
  spec.add_dependency 'mini_racer'
  
  spec.add_dependency 'timecop'
  spec.add_dependency 'email_spec'
  spec.add_dependency 'letter_opener'
  spec.add_dependency 'rack-mini-profiler'
  spec.add_dependency 'bullet'
  spec.add_dependency 'vcr'
  spec.add_dependency 'webmock'

  spec.add_dependency 'pry-byebug'
  spec.add_dependency 'binding_of_caller'
  spec.add_dependency 'better_errors'
  spec.add_dependency 'awesome_print'
  spec.add_dependency 'flamegraph'
  spec.add_dependency 'stackprof'

  spec.add_dependency 'dotenv-rails'
  
  spec.add_development_dependency "bundler", "~> 1.15"
  spec.add_development_dependency "rake", "~> 10.0"
end
