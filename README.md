# CiTools

Tools to help take the monotony out of CI and Testing Setup

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'ci_tools'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install ci_tools

## Usage

Drop into your gems/apps and start testing

## License

The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).
